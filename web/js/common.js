$(document).ready(function(){
    
    $('form').find('.autocomplete').each(function(){
        var singleresultUrl = $(this).attr('data_single_result');
        var autocompleteUrl = $(this).attr('data_multiple_result');
        
        var autocompleteFieldName = $(this).attr('name').replace('_autocomplete', '');
        var $autocompleteValueField = $(this).closest('form').find('input[name="' + autocompleteFieldName + '"]');
        
        $(this).selectize({
            valueField: 'value',
            labelField: 'label',
            searchField: 'label',
            options: [],
            items: [],
            maxItems: 1,
            create: false,
            onChange: function(value) {
              $autocompleteValueField.val(value);
            },
            onInitialize: function() {
                var self = this;
                var existingItem = null;
                var autocompletedValue = $autocompleteValueField.val();
                
                if(self.items.length > 0) {
                    for(var i=0; i<self.items.length; i++) {
                        existingItem = self.items[i];
                        self.removeItem(existingItem, true);
                        self.removeOption(existingItem);
                    }
                }
                self.refreshItems();
                                
                if(autocompletedValue != "") {
                    $.ajax({
                        url: singleresultUrl + '?id=' + autocompletedValue,
                        type: 'GET',
                        dataType: 'json',
                        success: function(res) {
                           self.addOption(res);
                           self.addItem(res.value);
                        }
                    })
                }
            },
            load: function(query, callback) {
                $.ajax({
                    url: autocompleteUrl + '?term=' + query,
                    type: 'GET',
                    dataType: 'json',
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);
                    }
                })
            }
        });
    });
});
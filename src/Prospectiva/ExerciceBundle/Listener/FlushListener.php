<?php
namespace Prospectiva\ExerciceBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Prospectiva\ExerciceBundle\Entity\SuiviMission;
use Prospectiva\ExerciceBundle\DBAL\EnumSuiviMissionStatut;

class FlushListener
{
    private $persistedEntities = array();
    
    public function postPersist(LifecycleEventArgs $eventArgs) {
        $persistedEntity = $eventArgs->getEntity();
        if($persistedEntity instanceof SuiviMission) {
            $this->persistedEntities['suivi'][] = $persistedEntity;
        }
    }
    
    public function postFlush(\Doctrine\ORM\Event\PostFlushEventArgs $eventArgs) {
        $em = $eventArgs->getEntityManager();
        $conn = $em->getConnection();
        
        if(isset($this->persistedEntities['suivi'])){
            
            //doctrine doesn't support join in update query :\
            //https://groups.google.com/forum/#!topic/doctrine-user/OaK0FrUItE8
            /*$reqMajStatutSuivi = $qb->update('AppBundle\Entity\SuiviMission', 'sm')
                ->leftJoin('AppBundle\Entity\Interimaire', 'i')
                ->set('sm.statut', $qb->expr()->literal(EnumSuiviMissionStatut::ENUM_SUPPRIME))
                ->where('sm.id_suivi_mission != :id_suivi_mission')
                ->andWhere('i.id_interimaire = :id_interimaire')
                ->getQuery();*/
            
            $dernierSuivi = array();
            foreach($this->persistedEntities['suivi'] as $entity) {
                $id_interimaire = $entity->getInterimaire()->getIdInterimaire();
                $id_suivi_mission = $entity->getIdSuiviMission();
                
                $dernierSuivi[$id_interimaire] = $id_suivi_mission;
            }
            
            foreach($dernierSuivi as $idInterimaire => $idSuiviMission) {
                $stmt = "UPDATE suivi_mission as sm"
                  . " LEFT JOIN interimaire as i ON (sm.id_interimaire = i.id_interimaire)"
                  . " SET sm.statut = " . $conn->quote(EnumSuiviMissionStatut::ENUM_SUPPRIME)
                  . " WHERE (sm.id_suivi_mission != " . $conn->quote($id_suivi_mission) . ")"
                  . " AND (i.id_interimaire = " . $conn->quote($id_interimaire) . ")";
                
                $conn->prepare($stmt)->execute();
                
            }
        }
        
        $this->persistedEntities = array();
    }
}
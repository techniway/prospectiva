<?php 
namespace Prospectiva\ExerciceBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Prospectiva\ExerciceBundle\DBAL\EnumContratStatut;
use Prospectiva\ExerciceBundle\DBAL\EnumSuiviMissionStatut;

use Prospectiva\ExerciceBundle\Entity\Interimaire;
use Prospectiva\ExerciceBundle\Entity\Contrat;
use Prospectiva\ExerciceBundle\Entity\SuiviMission;

class BundleFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {        
       $interimaire1 = new Interimaire();
       $interimaire1->setCodePostal('38360');
       $interimaire1->setVille('Sassenage');
       $interimaire1->setPrenom('Fabien');
       $interimaire1->setNom('Piron');
       $interimaire1->setEmail('contact@techniway.fr');
       $manager->persist($interimaire1);
       
       $contrat1 = new Contrat();
       $contrat1->setDebut(\DateTime::createFromFormat('Y-m-d', '2015-03-01'));
       $contrat1->setFin(\DateTime::createFromFormat('Y-m-d', '2018-02-01'));
       $contrat1->setInterimaire($interimaire1);
       $contrat1->setStatut(EnumContratStatut::ENUM_EN_COURS);
       $manager->persist($contrat1);
       
       $contrat2 = new Contrat();
       $contrat2->setDebut(\DateTime::createFromFormat('Y-m-d', '2018-03-01'));
       $contrat2->setFin(\DateTime::createFromFormat('Y-m-d', '2018-06-28'));
       $contrat2->setInterimaire($interimaire1);
       $contrat2->setStatut(EnumContratStatut::ENUM_TERMINE);
       $manager->persist($contrat2);
       
       $suivi1 = new SuiviMission();
       $suivi1->setContrat($contrat1);
       $suivi1->setInterimaire($interimaire1);
       $suivi1->setNote(9);
       $suivi1->setStatut(EnumSuiviMissionStatut::ENUM_SUPPRIME);
       $manager->persist($suivi1);
       
       $suivi2 = new SuiviMission();
       $suivi2->setContrat($contrat2);
       $suivi2->setInterimaire($interimaire1);
       $suivi2->setNote(7);
       $suivi2->setStatut(EnumSuiviMissionStatut::ENUM_ACTIF);
       $manager->persist($suivi2);
       
       $interimaire2 = new Interimaire();
       $interimaire2->setCodePostal('75010');
       $interimaire2->setVille('Paris');
       $interimaire2->setPrenom('Florent');
       $interimaire2->setNom('Pagny');
       $manager->persist($interimaire2);
       
       $contrat3 = new Contrat();
       $contrat3->setDebut(\DateTime::createFromFormat('Y-m-d', '2016-03-01'));
       $contrat3->setFin(\DateTime::createFromFormat('Y-m-d', '2017-07-25'));
       $contrat3->setInterimaire($interimaire2);
       $contrat3->setStatut(EnumContratStatut::ENUM_TERMINE);
       $manager->persist($contrat3);
       
       $suivi3 = new SuiviMission();
       $suivi3->setContrat($contrat3);
       $suivi3->setInterimaire($interimaire2);
       $suivi3->setNote(4);
       $suivi3->setStatut(EnumSuiviMissionStatut::ENUM_SUPPRIME);
       $manager->persist($suivi3);
       
       $interimaire3 = new Interimaire();
       $interimaire3->setCodePostal('38000');
       $interimaire3->setVille('Marseille');
       $interimaire3->setPrenom('Fabien');
       $interimaire3->setNom('Barthez');
       $manager->persist($interimaire3);
       
       $contrat4 = new Contrat();
       $contrat4->setDebut(\DateTime::createFromFormat('Y-m-d', '2016-01-01'));
       $contrat4->setFin(\DateTime::createFromFormat('Y-m-d', '2016-11-25'));
       $contrat4->setInterimaire($interimaire3);
       $contrat4->setStatut(EnumContratStatut::ENUM_TERMINE);
       $manager->persist($contrat4);
       
       $suivi4 = new SuiviMission();
       $suivi4->setContrat($contrat4);
       $suivi4->setInterimaire($interimaire3);
       $suivi4->setNote(6);
       $suivi4->setStatut(EnumSuiviMissionStatut::ENUM_SUPPRIME);
       $manager->persist($suivi4);
       
       $interimaire4 = new Interimaire();
       $interimaire4->setCodePostal('38000');
       $interimaire4->setPrenom('Flo');
       $interimaire4->setNom('Rida');
       $manager->persist($interimaire4);
       
       $manager->flush();
    }
}
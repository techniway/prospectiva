<?php

namespace Prospectiva\ExerciceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Prospectiva\ExerciceBundle\Repository\InterimaireRepository")
 * @ORM\Table(name="interimaire")
 */
class Interimaire
{  
    /**
     * @ORM\Id 
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     **/
   private $id_interimaire;
   
   /** 
    * @ORM\Column(type="string", nullable=false) 
    * **/
   private $nom;
   
    /** 
    * @ORM\Column(type="string", nullable=false) 
    * **/
   private $prenom;
   
    /** 
    * @ORM\Column(type="string", nullable=true) 
    * **/
   private $email;
   
    /** 
    * @ORM\Column(type="string", nullable=false) 
    * **/
   private $code_postal;
   
    /** 
    * @ORM\Column(type="string", nullable=true) 
    * **/
   private $ville;
   
   public function getIdInterimaire() {
       return $this->id_interimaire;
   }

   public function getNom() {
       return $this->nom;
   }

   public function getPrenom() {
       return $this->prenom;
   }

   public function getEmail() {
       return $this->email;
   }

   public function getCodePostal() {
       return $this->code_postal;
   }

   public function getVille() {
       return $this->ville;
   }
   
   public function setNom($nom) {
       $this->nom = $nom;
   }

   public function setPrenom($prenom) {
       $this->prenom = $prenom;
   }

   public function setEmail($email) {
       $this->email = $email;
   }

   public function setCodePostal($code_postal) {
       $this->code_postal = $code_postal;
   }

   public function setVille($ville) {
       $this->ville = $ville;
   }
}
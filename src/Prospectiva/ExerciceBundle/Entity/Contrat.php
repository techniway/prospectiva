<?php

namespace Prospectiva\ExerciceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use Prospectiva\ExerciceBundle\Entity\Interimaire;
use Prospectiva\ExerciceBundle\DBAL\EnumContratStatut;

/**
 * @ORM\Entity(repositoryClass="Prospectiva\ExerciceBundle\Repository\ContratRepository")
 * @ORM\Table(name="contrat")
 */
class Contrat
{    
    /**
     * @ORM\Id 
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     **/
   private $id_contrat;
      
   /** 
    * @ORM\Column(type="datetime", nullable=false) 
    * @Assert\NotBlank()
    * @Assert\DateTime()
    */
   private $debut;
   
   /** 
    * @ORM\Column(type="datetime", nullable=false) 
    * @Assert\NotBlank()
    * @Assert\DateTime()
    * @Assert\Expression(
    *   "this.getFin() > this.getDebut()",
    *   message="La date de fin doit être supérieur à la date de début"
    * )
    */
   private $fin;
   
   /** 
    * @ORM\Column(type="enumcontratstatut", nullable=false) 
    * @Assert\NotBlank()
    * @Assert\Choice(callback="getAuthorizedStatut")
    * 
    */
   private $statut;
   
   /**
    * @ORM\ManyToOne(targetEntity="Interimaire")
    * @ORM\JoinColumn(name="id_interimaire", referencedColumnName="id_interimaire", nullable=false)
    * @Assert\NotBlank()
    */
   private $interimaire;
   
   /**
    * @ORM\Column(type="boolean", options={"default" : false})
    */
   private $visible = true;
   
   public function getIdContrat() {
       return $this->id_contrat;
   }

   public function getDebut() {
       return $this->debut;
   }

   public function getFin() {
       return $this->fin;
   }

   public function getStatut() {
       return $this->statut;
   }

   public function getInterimaire() {
       return $this->interimaire;
   }
   
   public function getVisible() {
       return $this->visible;
   }

   public function setDebut(\DateTime $debut) {
       $this->debut = $debut;
   }

   public function setFin(\DateTime $fin) {
       $this->fin = $fin;
   }

   public function setStatut($statut) {
       $this->statut = $statut;
   }

   public function setInterimaire(Interimaire $interimaire) {
       $this->interimaire = $interimaire;
   }
   
   public function setVisible($visible) {
       $this->visible = $visible;
   }

   public static function getAuthorizedStatut()
   {
        return EnumContratStatut::getValues();
   }
}
<?php

namespace Prospectiva\ExerciceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;

use Prospectiva\ExerciceBundle\Entity\Interimaire;
use Prospectiva\ExerciceBundle\Entity\Contrat;
use Prospectiva\ExerciceBundle\DBAL\EnumSuiviMissionStatut;

/**
 * @ORM\Entity
 * @ORM\Table(name="suivi_mission")
 */
class SuiviMission
{    
    /**
     * @ORM\Id 
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     **/
   private $id_suivi_mission;
      
   /** @ORM\Column(type="smallint", nullable=false) */
   private $note;
   
   /** @ORM\Column(type="enumsuivimissionstatut", nullable=false) */
   private $statut;
   
   /**
    * @ORM\ManyToOne(targetEntity="Interimaire")
    * @ORM\JoinColumn(name="id_interimaire", referencedColumnName="id_interimaire", nullable=false)
    */
   private $interimaire;
   
   /**
    * @ORM\ManyToOne(targetEntity="Contrat")
    * @ORM\JoinColumn(name="id_contrat", referencedColumnName="id_contrat", nullable=false)
    */
   private $contrat;
   
   public function __construct() {
       $this->statut = EnumSuiviMissionStatut::ENUM_ACTIF;
   }
   
   public function getIdSuiviMission() {
       return $this->id_suivi_mission;
   }

   public function getNote() {
       return $this->note;
   }

   public function getStatut() {
       return $this->statut;
   }

   public function getInterimaire() {
       return $this->interimaire;
   }

   public function getContrat() {
       return $this->contrat;
   }

   public function setNote($note) {
       $this->note = $note;
   }

   public function setStatut($statut) {
       $this->statut = $statut;
   }

   public function setInterimaire(Interimaire $interimaire) {
       $this->interimaire = $interimaire;
   }

   public function setContrat(Contrat $contrat) {
       $this->contrat = $contrat;
   }
}
<?php

namespace Prospectiva\ExerciceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Prospectiva\ExerciceBundle\Entity\Interimaire;
use Prospectiva\ExerciceBundle\Entity\Contrat;
use Prospectiva\ExerciceBundle\Form\ContratType;

class InterimaireController extends Controller
{
    /**
     * @Route("/getInterimaire", name="getInterimaire")
     */
    public function getInterimaire(Request $request) {
        $result = array();
        
        $id = $request->query->get('id');
        if($id !== null) {
            $interimaire = $this->getDoctrine()->getRepository(Interimaire::class)
            ->find($id);
            
            if($interimaire !== null) {
                $result = array(
                    'label' => ucfirst($interimaire->getPrenom()) . ' ' . strtoupper($interimaire->getNom()),
                    'value' => $interimaire->getIdInterimaire()
                );
            }
        }
        
        return new \Symfony\Component\HttpFoundation\JsonResponse($result);
    }
    
    /**
     * @Route("/search", name="searchInterimaire")
     */
    public function searchInterimaire(Request $request) {
        $result = array();
        
        $term = $request->query->get('term');
        if($term !== null) {
           $interimaires = $this->getDoctrine()->getRepository(Interimaire::class)
                    ->getInterimaireWithBestMark($term);
           
           if(count($interimaires) > 0) {
               foreach($interimaires as $item) {
                   $result[] = array(
                       'label' => ucfirst($item->getPrenom()) . ' ' . strtoupper($item->getNom()),
                       'value' => $item->getIdInterimaire()
                   );
               }
           }
        }
                
        return new \Symfony\Component\HttpFoundation\JsonResponse($result);
    }
}

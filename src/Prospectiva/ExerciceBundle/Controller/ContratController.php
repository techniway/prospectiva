<?php

namespace Prospectiva\ExerciceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Prospectiva\ExerciceBundle\Entity\Contrat;
use Prospectiva\ExerciceBundle\Form\ContratType;

class ContratController extends Controller
{
    /**
     * @Route("/", name="listContrat")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $contrats = $em->getRepository(Contrat::class)->listContratWithInterimaire();
         
        return $this->render('@ProspectivaExercice/contrat/index.html.twig', array(
            'contrats' => $contrats
        ));
    }
        
    /**
     * @Route("/edit/{id_contrat}", name="editContrat", requirements={"id_contrat"="\d+"})
     * @Route("/create", name="createContrat")
     */
    public function updateAction(Request $request, $id_contrat = null)
    {
        $em = $this->getDoctrine()->getManager();
        $contrat = new Contrat();
        
        if($id_contrat !== null) {                        
            $requestedContrat = $em->getRepository(Contrat::class)->getContratWithInterimaire($id_contrat);
            $contrat = $requestedContrat !== null ? $requestedContrat : $contrat;
        }
                
        $singleResultRoute = $this->generateUrl('getInterimaire');
        $multipleResultRoute = $this->generateUrl('searchInterimaire');
        
        $form = $this->createForm(ContratType::class, $contrat, array(
            'single_result_route' => $singleResultRoute,
            'multiple_result_route' => $multipleResultRoute
        ));
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $formData = $form->getData();
            
            $em->persist($formData);
            $em->flush();
            
            $this->addFlash("success", "Vos modifications ont bien été prises en compte");
            if($id_contrat !== null) {
                return $this->redirectToRoute("editContrat", array('id_contrat' => $id_contrat));
            } else {
                return $this->redirectToRoute("listContrat");
            }
        }
                
        $response = $this->render('@ProspectivaExercice/contrat/update.html.twig', array(
            'form' => $form->createView(),
            'contrat' => $contrat
        ));
        
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('max-age', 0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);
        
        return $response;
    }
    
    /**
     * @Route("/delete/{id_contrat}", name="deleteContrat")
     */
    public function deleteAction(Request $request, $id_contrat = null)
    {
        $em = $this->getDoctrine()->getManager();
        $contrat = $em->getRepository(Contrat::class)->find($id_contrat);
        
        if($contrat !== null) {
            $contrat->setVisible(false);
            $em->persist($contrat);
            $em->flush();
            
            $this->addFlash("success", "Vos modifications ont bien été prises en compte");
        }
        
        return $this->redirectToRoute("listContrat");
    }
}

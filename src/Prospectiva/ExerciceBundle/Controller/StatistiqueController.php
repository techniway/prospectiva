<?php

namespace Prospectiva\ExerciceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Prospectiva\ExerciceBundle\Form\StatistiqueType;
use Prospectiva\ExerciceBundle\Entity\Contrat;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class StatistiqueController extends Controller
{
    /**
     * @Route("/getContrat", name="getContratStatistique")
     */
    public function getContratAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(StatistiqueType::class);
        $form->handleRequest($request);
                
        if($form->isSubmitted() && $form->isValid()){
            $formData = $form->getData();
            $debut = $formData['debut'];
            $fin = $formData['fin'];
            $contrats = $em->getRepository(Contrat::class)->getContratStatistique($debut, $fin);
                        
            $encoders= array(new CsvEncoder());
            $normalizers = array(new DateTimeNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            
            $csvData = $serializer->normalize($contrats);
            $csvData = $serializer->encode($csvData, 'csv');
            
            return new Response($csvData, 200, array(
                'Content-Type' => 'text/csv',
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => 'attachment; filename="'.urlencode('statistique.csv')."",
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate',
                'Pragma' => 'public',
            ));
        }
        
        return $this->render('@ProspectivaExercice/statistique/contrat.html.twig', array(
            'form' => $form->createView()
        ));
    }
}

<?php

namespace Prospectiva\ExerciceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Doctrine\Common\Persistence\ObjectManager;
use Prospectiva\ExerciceBundle\Form\EntityHiddenTransformer;
use Prospectiva\ExerciceBundle\DBAL\EnumContratStatut;
use Prospectiva\ExerciceBundle\Entity\Interimaire;
use Prospectiva\ExerciceBundle\Entity\Contrat;


class ContratType extends AbstractType
{
    private $_objectManager;
    
    public function __construct(ObjectManager $objectManager)
    {
        $this->_objectManager = $objectManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $hiddenTransformer = new EntityHiddenTransformer(
            $this->_objectManager,
            Interimaire::class,
            'id_interimaire'
        );
        
        $attr = array('class' => 'autocomplete');
        $attr['data_single_result'] = $options['single_result_route'];
        $attr['data_multiple_result'] = $options['multiple_result_route'];
        $attr['autocomplete'] = 'disabled';
        
        $builder
            ->add(
               $builder->create('interimaire', HiddenType::class)
               ->addModelTransformer($hiddenTransformer)
            )
            ->add('interimaire_autocomplete', TextType::class, array(
                'label' => 'Interimaire',
                'attr'=> $attr,
                'mapped' => false
             ))
            ->add('debut', DateType::class, array(
                'widget' => 'choice'
             ))
            ->add('fin', DateType::class, array(
                'widget' => 'choice'
             ))
            ->add('statut', ChoiceType::class , array(
                'choices' => EnumContratStatut::getLabel()
             ))
            ->add('enregistrer', SubmitType::class, array('label' => 'Enregistrer', 
                'attr' => array('class' => 'btn-primary')
             ));
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array('single_result_route', 'multiple_result_route'));
        $resolver->setDefaults(array(
            'data_class' => Contrat::class,
        ));
    }
}
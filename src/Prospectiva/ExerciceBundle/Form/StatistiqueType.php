<?php

namespace Prospectiva\ExerciceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Expression;



class StatistiqueType extends AbstractType
{       
    public function buildForm(FormBuilderInterface $builder, array $options)
    {           
        $builder
            ->add('debut', DateType::class, array(
                'widget' => 'choice',
                'constraints' => array(
                    new NotBlank()
                )
             ))
            ->add('fin', DateType::class, array(
                'widget' => 'choice',
                'constraints' => array(
                    new NotBlank(),
                )
             ))
            ->add('enregistrer', SubmitType::class, array('label' => 'Enregistrer', 
                'attr' => array('class' => 'btn-primary')
             ));
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'constraints' => [
                new Expression([
                    'expression' => 'value["fin"] > value["debut"]',
                    'message' => 'La date de fin doit être supérieur à la date de début'
                ])
            ],
        ]);
    }
}
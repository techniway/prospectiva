<?php

namespace Prospectiva\ExerciceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Prospectiva\ExerciceBundle\Entity\Interimaire;

class GeocodingMissingAddressCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('app:geocode-missing-address')
            ->setDescription('geocode les adresses manquantes des intérimaires');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbInterimaireUpdated = 0;
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
                
        $geocodingUrl = $this->getContainer()->getParameter('prospectiva_exercice.geocoding_url');
        $geocodingKey = $this->getContainer()->getParameter('prospectiva_exercice.geocoding_key');
        
        
        $interimairesWithoutCity = $em->getRepository(Interimaire::class)
            ->getInterimaireWithoutCity();
                
        /**
         * TODO il vaudrait mieux avoir une ville pour géocoder correctemment le code postal
         * et même pour le reste mais pour un exercice ça ira ;)
         */
        foreach($interimairesWithoutCity as $interimaire) {
            $geocodedCity = null;
            $geocodingAdress = urlencode($interimaire->getCodePostal());
            $geocodingQuery = $geocodingUrl . '/json?address=' . $geocodingAdress
                    . '&region=fr' . '&key=' . $geocodingKey;
            
            $response = file_get_contents($geocodingQuery);
            if($response !== false) {
                $response = json_decode($response, true);
                                                
                if($response['status'] == 'OK' && count($response['results']) > 0) {
                    foreach($response['results'][0]['address_components'] as $component) {
                        $diff = array_diff(array('locality'), $component['types']);
                        
                        if(count($diff) == 0){
                            $geocodedCity = $component['long_name'];
                            break;
                        }
                    }
                }
                
                if($geocodedCity !== null) {
                    $em->createQueryBuilder()
                      ->update('Prospectiva\ExerciceBundle\Entity\Interimaire', 'i')
                      ->set('i.ville', ':ville')
                      ->where('i.id_interimaire = :id_interimaire')
                      ->setParameter('ville', $geocodedCity)
                      ->setParameter('id_interimaire', $interimaire->getIdInterimaire())
                      ->getQuery()
                      ->execute();
                    
                    $nbInterimaireUpdated++;
                }
            }
        }
        
        $output->writeln("nombre d'intérimaire mise à jour: " . $nbInterimaireUpdated);
    }
}
<?php

namespace Prospectiva\ExerciceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Prospectiva\ExerciceBundle\Entity\Contrat;
use Prospectiva\ExerciceBundle\DBAL\EnumContratStatut;

class TerminateOldContratCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('app:terminate-old-contrat')
            ->setDescription('met a terminé le statut des contrats dont la date a été dépassé');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbContratUpdated = 0;
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
                
        $obsoleteContrats = $em->getRepository(Contrat::class)->getObsoleteContract();
                        
        foreach($obsoleteContrats as $contrat) {
           $contrat->setStatut(EnumContratStatut::ENUM_TERMINE);
           $em->persist($contrat);
           
           $nbContratUpdated++;
        }
        
        $em->flush();
        $output->writeln("nombre de contrat mise à jour: " . $nbContratUpdated);
    }
}
<?php

namespace Prospectiva\ExerciceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Swift_Message;

use Prospectiva\ExerciceBundle\Entity\Contrat;

class SendNotificationToInterimaireCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('app:send-notification-to-interimaire')
            ->setDescription('envoie une notification aux intérimaires');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nbMailSent = 0;
        
        $doctrine = $this->getContainer()->get('doctrine');
        $mailer = $this->getContainer()->get('swiftmailer.mailer.default');
        $em = $doctrine->getEntityManager();
        
        $contratsBeginningSoon = $em->getRepository(Contrat::class)->getContratBeginningSoon();
        
        foreach($contratsBeginningSoon as $contrat) {
            if($contrat->getInterimaire()->getEmail() !== null) {
                $message = new Swift_Message('Votre contrat commence bientôt');
                $message->setFrom('contact@techniway.fr')
                  ->setTo($contrat->getInterimaire()->getEmail())
                  ->setBody(
                       '<h1>Votre nouveau contrat commence bientôt</h1><p>N\'oubliez pas de mettre le réveil</p>',
                       'text/html'
                );

               $mailer->send($message);
               $nbMailSent++;
            }
        }
      
        $output->writeln("nombre d'email envoyés" . $nbMailSent);
    }
}
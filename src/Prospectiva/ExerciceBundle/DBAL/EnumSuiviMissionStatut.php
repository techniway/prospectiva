<?php
namespace Prospectiva\ExerciceBundle\DBAL;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Prospectiva\ExerciceBundle\DBAL\EnumType;

class EnumSuiviMissionStatut extends EnumType
{
    protected $name = 'enumsuivimissionstatut';
    
    CONST ENUM_ACTIF = 'ACTIF';
    CONST ENUM_SUPPRIME = 'SUPPRIME';
}
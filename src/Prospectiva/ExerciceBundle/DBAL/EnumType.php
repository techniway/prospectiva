<?php
namespace Prospectiva\ExerciceBundle\DBAL;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

abstract class EnumType extends Type
{
    protected $name;
    protected $values = array();
        
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $this->initValues();
        $values = array_map(function($val) { return "'".$val."'"; }, $this->values);

        return "ENUM(".implode(", ", $values).")";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $this->initValues();
        if (!in_array($value, $this->values)) {
            throw new \InvalidArgumentException("Invalid '".$this->name."' value.");
        }
        return $value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
    
    public static function getValues()
    {
        $reflector = new \ReflectionClass(get_called_class());
        $constantName = 'ENUM_';
        $values = array();
        
        foreach($reflector->getConstants() as $key => $value) {
            if(strstr($key, $constantName) !== false) {
                $values[] = $value;
            }
        }
        
        return $values;
    }
    
    public static function getLabel()
    {
        $reflector = new \ReflectionClass(get_called_class());
        $labels = array();
        $values = array();
        $custom_labels = array();
        
        $constantName = 'ENUM_';
        foreach($reflector->getConstants() as $key => $value){                
            if(strstr($key, $constantName) !== false) {
                $values[] = $value;
            }
            
            if(strstr($key, 'CUSTOM_LABEL') !== false) {
                $custom_labels = $value;
            }
        }
                
        $bHasCustomLabel = count($custom_labels) > 0 ? true : false;
        foreach($values as $key) {
            if($bHasCustomLabel) {
                $labels[$custom_labels[$key]] = $key;
            } else {
                $labels[$key] = $key;
            }
        }
        
        return $labels;
    }
        
    private function initValues() {        
        if(count($this->values) == 0) {
            $this->values = self::getValues();
        }
    }
}
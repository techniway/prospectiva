<?php
namespace Prospectiva\ExerciceBundle\DBAL;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Prospectiva\ExerciceBundle\DBAL\EnumType;

class EnumContratStatut extends EnumType
{
    protected $name = 'enumcontratstatut';
    
    const ENUM_EN_ATTENTE = 'EN_ATTENTE';
    const ENUM_EN_COURS = 'EN_COURS';
    const ENUM_TERMINE = 'TERMINE';
}
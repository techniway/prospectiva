<?php

namespace Prospectiva\ExerciceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Prospectiva\ExerciceBundle\DBAL\EnumContratStatut;

class ContratRepository extends EntityRepository
{    
    public function getContratWithInterimaire($id_contrat)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        return $qb->select('c')
            ->from('Prospectiva\ExerciceBundle\Entity\Contrat', 'c')
            ->leftJoin('c.interimaire', 'i')
            ->addSelect('i')
            ->where('c.id_contrat = :id_contrat')
            ->setParameter('id_contrat', $id_contrat)
            ->getQuery()
            ->getOneOrNullResult();
    }
    
    public function getContratStatistique(\DateTime $debut, \DateTime $fin, $hydratationMode = \Doctrine\ORM\Query::HYDRATE_OBJECT) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $debut = $debut->format('Y-m-d');
        $fin = $fin->format('Y-m-d');
        
        return $qb->select('i.nom, i.prenom, c.debut, c.fin, c.statut')
            ->from('Prospectiva\ExerciceBundle\Entity\Contrat', 'c')
            ->leftJoin('c.interimaire', 'i')
            ->where('c.debut >= :debut and c.fin <= :fin')
            ->setParameter('debut', $debut) 
            ->setParameter('fin', $fin)
            ->getQuery()
            ->getResult($hydratationMode);
    }
    
    public function getContratBeginningSoon($nbrDayBefore = 1)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        return $qb->select('c')
            ->from('Prospectiva\ExerciceBundle\Entity\Contrat', 'c')
            ->leftJoin('c.interimaire', 'i')
            ->addSelect('i')
            ->where('DATE_DIFF(c.debut, CURRENT_DATE()) = :nbrDayBefore')
            ->setParameter('nbrDayBefore', $nbrDayBefore)
            ->getQuery()
            ->getResult();
    }
    
    public function getObsoleteContract()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        return $qb->select('c')
            ->from('Prospectiva\ExerciceBundle\Entity\Contrat', 'c')
            ->where('DATE_DIFF(CURRENT_DATE(), c.fin) > 0 AND c.statut != :obsoleteStatut')
            ->setParameter('obsoleteStatut', EnumContratStatut::ENUM_TERMINE)
            ->getQuery()
            ->getResult();
    }
    
    public function listContratWithInterimaire($hydratationMode = \Doctrine\ORM\Query::HYDRATE_OBJECT)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        return $qb->select('c')
            ->from('Prospectiva\ExerciceBundle\Entity\Contrat', 'c')
            ->leftJoin('c.interimaire', 'i')
            ->addSelect('i')
            ->where('c.visible = true')
            ->getQuery()
            ->getResult($hydratationMode);
    }
}


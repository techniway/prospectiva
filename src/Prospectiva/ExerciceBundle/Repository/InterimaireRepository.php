<?php

namespace Prospectiva\ExerciceBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InterimaireRepository extends EntityRepository
{    
    public function getInterimaireWithBestMark($name = null, $hydratationMode = \Doctrine\ORM\Query::HYDRATE_OBJECT) {
        $interimaires = array();
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $query = $qb->select('i')
           ->addSelect('SUM(sm.note) as note')
           ->from('Prospectiva\ExerciceBundle\Entity\SuiviMission', 'sm')
           ->join('Prospectiva\ExerciceBundle\Entity\Interimaire', 'i', 'WITH', 'sm.interimaire = i');
        
        if(isset($name)) {
            $query->where("concat(i.prenom, ' ', i.nom) like :term");
            $query->setParameter('term', $name.'%');
        }
        
        $query->groupBy('i.id_interimaire')
           ->orderBy('note', 'DESC');
        
        $res = $query->getQuery()
           ->getResult($hydratationMode);
                
        foreach($res as $item) {
            $interimaires[] = $item[0];
        }
                
        return $interimaires;
    }
    
    public function getInterimaireWithoutCity() {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        return $qb->select('i')
           ->from("Prospectiva\ExerciceBundle\Entity\Interimaire", "i")
           ->where("i.ville IS NULL OR i.ville = '")
           ->getQuery()
           ->getResult();
    }
}


<?php

namespace Prospectiva\ExerciceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Events;
use Prospectiva\ExerciceBundle\Listener\FlushListener;

class ProspectivaExerciceBundle extends Bundle
{
    public function boot()
    {            
        $em = $this->container->get('doctrine.orm.default_entity_manager');
                
        if(Type::hasType('enumcontratstatut') === false) {
            Type::addType('enumcontratstatut', 'Prospectiva\ExerciceBundle\DBAL\EnumContratStatut');
            $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enumcontratstatut', 'enumcontratstatut');
        }
        
        if(Type::hasType('enumsuivimissionstatut') === false) {
            Type::addType('enumsuivimissionstatut', 'Prospectiva\ExerciceBundle\DBAL\EnumSuiviMissionStatut');
            $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enumsuivimissionstatut', 'enumsuivimissionstatut');
        }
        
        $flushListener = new FlushListener();
        $em->getConnection()->getEventManager()->addEventListener([Events::postPersist], $flushListener);
        $em->getConnection()->getEventManager()->addEventListener([Events::postFlush], $flushListener);
    }
}
